package com.example.practica02java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText txtPeso;
    private EditText txtAltura;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private TextView lblResultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtPeso = (EditText)findViewById(R.id.txtPeso);
        txtAltura = (EditText)findViewById(R.id.txtAltura);

        btnCalcular = (Button)findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        lblResultado = (TextView)findViewById(R.id.lblResultado);

        //Boton para mandar a llamar la funcion de calcularIMC
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtAltura.getText().toString().matches("")){
                    //Falta capturar la altura
                    Toast.makeText(MainActivity.this,
                            "Favor de llenar los campos",Toast.LENGTH_SHORT).show();
                }else if(txtPeso.getText().toString().matches("")) {
                    //Falta capturar el peso
                    Toast.makeText(MainActivity.this,
                            "Favor de llenar los campos",Toast.LENGTH_SHORT).show();
                }else {
                    calcularIMC();
                }
            }
        });

        //Boton de limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtPeso.setText("");
                txtAltura.setText("");
                lblResultado.setText("");
            }
        });

        //Boton para cerrar la app
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularIMC() {
        double peso = Double.parseDouble(txtPeso.getText().toString());
        double altura = Double.parseDouble(txtAltura.getText().toString());

        double imc = peso / (altura * altura);

        lblResultado.setText("Tu IMC es: " + String.format("%.2f", imc));
    }


}